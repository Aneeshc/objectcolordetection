From python:3.6.4
RUN mkdir /home/app
WORKDIR /home/app
ADD . /home/app
RUN cd /home/app
RUN pip install --upgrade pip 
RUN pip install Cython
RUN pip install numpy==1.14.5
RUN pip install -r requirement.txt 

CMD python detect.py

#cython==0.28.5
