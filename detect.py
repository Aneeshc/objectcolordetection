from imageai.Detection import ObjectDetection
from mrcnn import utils
from skimage import io

import numpy as np
import cv2
from sklearn.cluster import KMeans
import matplotlib.pylab as plt
import matplotlib.patches as patches

from colorlabeler import ColorLabeler
from maskrcnn import *

import pandas as pd
import warnings
import pymysql



warnings.filterwarnings("ignore")

cl = ColorLabeler()




def extract_patch_mask(img, array):
    x1, y1, x2, y2 = array[0], array[1], array[2], array[3]
    sub_img = img[y1-10:y2+10, x1-10:x2+10]
    return sub_img




def get_database_connection():
    try:
        database = pymysql.connect(
        host="kapowdb.gogocar.com",
        user="kapow_root",
        passwd="K@nn@Pp@NUnN!!",
        db="AI_data_hub")
        return database
    except:
        get_database_connection()



def url_to_image(url):
   # url = 'https://s3.us-west-2.amazonaws.com/gogocar-dev/media/vehicles/detailsphoto/lg1/3TMDZ5BN8JM037857_1.jpg'
    try:
        image = io.imread(url)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return True,image
    except:
        return  False,None



def get_data_fromdb(df,query,limit = 1000,offset = 0):

    conn = get_database_connection()
    dbquery = query + " limit %s offset %s "%( str(limit),str(offset) )
    print(dbquery)
    try:
        dbresult = pd.read_sql(dbquery,conn)
        conn.close()
    except:
        dbresult = pd.read_sql(dbquery,conn)
        conn.close()

    if(len(dbresult) == 0):
        return df
    df = df.append([dbresult])
    offset = limit+offset
    return get_data_fromdb(df,query,limit,offset)



def make_segmentation_mask(image, mask):
    img = image.copy()
    img[:,:,0] *= mask
    img[:,:,1] *= mask
    img[:,:,2] *= mask
    return img


def find_pixels(rcnn_result, mask):

    #RGB = average_colour(rcnn_result)
    list_intensities = []
    mask = mask*1
    pts = np.where(mask == 1)
    #print ('pts', pts)
    
    for ind, vals in enumerate(pts[0]):
        list_intensities.append(rcnn_result[pts[0][ind], pts[1][ind]])

    return list_intensities






def updateDB(flag, row, RGB, Color):
    print(flag, RGB, Color)
    connection = get_database_connection()
    cursor = connection.cursor()
    query = "UPDATE AI_data_hub.color_recognition SET GenericColor2= '%s' , Hexcode = '%s' , RecognitionStatus2= '%s' \
WHERE Vin='%s' "%(Color, RGB, flag, row.Vin)
    cursor.execute(query)
    cursor.close()
    connection.commit()
    connection.close()

def kmeans_mingrad(patch):
    img_vec = np.reshape(patch, [patch.shape[0] * patch.shape[1], patch.shape[2]] )

    kmeans = KMeans(n_clusters=3)
    kmeans.fit( img_vec )
    unique_l, counts_l = np.unique(kmeans.labels_, return_counts=True)
    sort_ix = np.argsort(counts_l)
    sort_ix = sort_ix[::-1]

    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    # x_from = 0.05

    # for cluster_center in kmeans.cluster_centers_[sort_ix]:
    #     ax.add_patch(patches.Rectangle( (x_from, 0.05), 0.29, 0.9, alpha=None,
    #                                     facecolor='#%02x%02x%02x' % (int(cluster_center[2]), int(cluster_center[1]), int(cluster_center[0]) ) ) )
    #     x_from = x_from + 0.31

    # plt.show()
    return kmeans.cluster_centers_[sort_ix][0]


def find_grad(rcnn_result):
    from sklearn.feature_extraction import image
    height, width = rcnn_result.shape[0]/2, rcnn_result.shape[1]/2

    height_thresh = 0.50 * height
    width_thresh = 0.90* width

    sub = rcnn_result[int(height-height_thresh):int(height+height_thresh), int(width-width_thresh):int(width+width_thresh)]
    #print (type(sub))
    # cv2.imshow('Original Image', sub)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
    patches = image.extract_patches_2d(sub, (int(sub.shape[0]/2), int(sub.shape[1]/2)) , max_patches = 150)


    temp = 0

    for patch in patches:
        im = np.float32(patch) / 255.0
        gx = cv2.Sobel(im, cv2.CV_32F, 1, 0, ksize=1)
        gy = cv2.Sobel(im, cv2.CV_32F, 0, 1, ksize=1)
        mag, angle = cv2.cartToPolar(gx, gy, angleInDegrees=True)
        if temp == 0:
            temp = mag.sum()
        if mag.sum()<=temp:
            temp = mag.sum()
            frame = patch
           # print ('inside')

    return frame


if __name__ == "__main__":
    table = get_data_fromdb( pd.DataFrame(),"SELECT * FROM AI_data_hub.color_recognition where RecognitionStatus2 is null",2050,0 )

    for index,row in table.iterrows():
        imageURL = row
        print(index,' ', row.ImageUrl)
        flag, images = url_to_image(row.ImageUrl)
        if not flag:
            updateDB(str(flag), row, 'image not avialable', '')
            continue
        #image = loadImage()
        img = images.copy()

        execution_path = os.getcwd()
        #img = cv2.imread(os.path.join(execution_path, file_name))
        detector = ObjectDetection()
        detector.setModelTypeAsRetinaNet()
        detector.setModelPath(os.path.join(execution_path, "resnet50_coco_best_v2.0.1.h5"))
        detector.loadModel()
        detections = detector.detectObjectsFromImage(input_image=img,
                                                     output_image_path=
                                                     os.path.join(execution_path, "imagenew.jpg"),
                                                     input_type='array')

        temp = 0

        if len(detections) !=0:
            for eachObject in detections:
                if eachObject['name'] == 'car' or eachObject['name'] == 'truck':
                    area = (eachObject['box_points'][2]- eachObject['box_points'][0])+ (eachObject['box_points'][3]- eachObject['box_points'][1])
                    if area>temp:
                        temp = area
                        array = eachObject['box_points']
        else:
            array = []


       
        if len(array) !=0:
          
            
            sub_img = extract_patch_mask(img, array)
            cv2.imwrite('sub.jpg', sub_img)
            results = model.detect([sub_img], verbose=1)
            r = results[0]
            a = 0
            print (r['masks'].shape[2])
            for ele in range(r['masks'].shape[2]):
                temp = r['masks'][:, :, ele]
                temp = temp*1
                arr = temp.sum()
                print ('arr', arr)
                if arr>a:
                    a = arr
                    mask = r['masks'][:, :, ele]
            # cv2.imshow('grad result', sub_img)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()
            rcnn_result = make_segmentation_mask(sub_img, mask)
            # cv2.imshow('grad result', rcnn_result)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()



            mingrad = find_grad(rcnn_result)

            rgb_km = kmeans_mingrad(mingrad)
            rgb_km = rgb_km[::-1]
            
            val2 = cl.label(rgb_km)

            print ('df2', val2)
            # cv2.imshow('grad result', mingrad)

            print ('km rgb', rgb_km)

            RGB = [int(i) for i in rgb_km]
            RGB_str = '-'.join(map(str, RGB))
            print(val2['Generic Color Name'])
            updateDB(str(flag), row, RGB_str, val2['Generic Color Name'])
          


        else:
            updateDB(str(flag), row, 'No car found', '')
            print ('No car found')

















